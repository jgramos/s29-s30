//[SECTION] Create an API service using Express

//Identify the proper ingredient/materials/components needed to start the project.

	//use the 'require' directive to load the express module/package

const express = require('express')
const application = express();

	//identify a virtual port in which to serve the project.
const port = 4000;

	//assign the established connection/server into the desginated port.
application.listen(port, () => console.log(`Server is running on ${port}`));